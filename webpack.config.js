const webpack = require('webpack');
const path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'build');
var APP_DIR = path.resolve(__dirname, 'src');

var config = {
  entry: [
    //'webpack-dev-server/client?http://0.0.0.0:3000',
    //'webpack/hot/only-dev-server',
    APP_DIR + '/app.coffee'
  ],
  output: {
    path: BUILD_DIR,
    filename: 'index.js'
  },
  module: {
    loaders: [{
      test: /\.coffee$/,
      exclude: /node_modules/,
      //include: APP_DIR,
      loader: 'coffee',
      //loaders: ['react-hot']
    }]
  },
  resolve: {
    extensions: ["", ".web.coffee", ".web.js", ".coffee", ".js"]
  }
  //plugins: [
  //    new webpack.HotModuleReplacementPlugin()
  //]
};

module.exports = config;
