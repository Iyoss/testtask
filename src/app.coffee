'use strict'
React = require 'react'
ReactDOM =  require 'react-dom'
App = require './components/App.coffee'
Bundles = require './bundles/bundles.coffee'

ReactDOM.render React.createElement(Bundles),
  document.getElementById('example')
