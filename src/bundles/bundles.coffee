'use strict'
React = require 'react'
BundleCompany = require './bundle-company/BundleCompany.coffee'
BundlePerson = require './bundle-person/BundlePerson.coffee'

Bundles = React.createClass
  render: ->
    React.DOM.section
      className: ''
      React.createElement BundleCompany
      React.createElement BundlePerson

module.exports = Bundles
