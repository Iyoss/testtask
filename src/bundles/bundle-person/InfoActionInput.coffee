'use strict'
React = require 'react'

InfoActionInput = React.createClass
  getInitialState: ->
    placeholder: 'Add ' + @props.text
    value: ''

  handleOnFocus: ->
    if !@state.value
      @setState placeholder: ''

  handleOnBlur: ->
    if !@state.value
      @setState placeholder: 'Add ' + @props.text

  handleChange: (event) ->
    @setState value: event.target.value.substr 0, 60

  handlerOnKeyPress: (event) ->
    if event.charCode is 13 and @state.value
      @props.addContacts @props.column, @state.value
      @setState value: ''

  render: ->
    React.DOM.div
      className: 'info-section__action'
      React.DOM.p
        className: 'action-item'
        React.DOM.input
          type: 'text'
          value: @state.value
          placeholder: @state.placeholder
          className: 'input--bundle-man-info'
          onChange: @handleChange
          onFocus: @handleOnFocus
          onBlur: @handleOnBlur
          onKeyPress: @handlerOnKeyPress
        React.DOM.i
          className: 'is is-' + @props.icon

module.exports = InfoActionInput
