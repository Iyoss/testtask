'use strict'
React = require 'react'
ReactCSSTransitionGroup = require 'react-addons-css-transition-group'
DragDrop = require 'react-dnd'
HTML5Backend = require 'react-dnd-html5-backend'
DragDropContext = DragDrop.DragDropContext
BundleInfoOneEntry = require './InfoOneEntry.coffee'
InfoActionInput = require './InfoActionInput.coffee'

BundleInfoPerson = React.createClass


  createCols: (info, key) ->
    React.DOM.div
      key: key
      className: 'bundle-info__section'
      React.DOM.ul
        className: ''
        React.createElement ReactCSSTransitionGroup,
          transitionName: 'slideUp'
          transitionEnterTimeout: 300
          transitionLeaveTimeout: 300
          for _d, _i in info.contacts
            React.createElement BundleInfoOneEntry,
              key: _d.id
              index: _i
              column: key
              id: _d.id
              text: _d.text
              icon: info.icon
              section: info.section
              moveInfo: @props.moveInfo
              editContacts: @props.editContacts
              removeContacts: @props.removeContacts
        React.createElement InfoActionInput,
          icon: info.icon
          text: info.add_btn_text
          column: key
          addContacts: @props.addContacts

  render: ->
    style = 'bundle__info'
    style = 'bundle__info open' if @props.is_show_info

    React.DOM.div
      className: style
      for d, i in @props.info
        @createCols(d, i)

module.exports = DragDropContext(HTML5Backend) BundleInfoPerson
