'use strict'
React = require 'react'
ReactDOM = require 'react-dom';
findDOMNode = ReactDOM.findDOMNode;
DragDrop = require 'react-dnd'
PropTypes = React.PropTypes
DragSource = DragDrop.DragSource
DropTarget = DragDrop.DropTarget

ItemTypes =
  CARD: 'card'

cardSource =
  beginDrag: (props) ->
    id: props.id
    index: props.index
    column: props.column

cardTarget =
  hover: (props, monitor, component) ->
    dragIndex = monitor.getItem().index
    dragColumn = monitor.getItem().column
    hoverIndex = props.index
    columnIndex = props.column

    #    Don't to drag in another column
    if dragColumn != columnIndex
      return false

    #    Don't replace items with themselves
    if dragIndex is hoverIndex
      return false

    #    Determine rectangle on screen
    hoverBoundingRect = findDOMNode(component).getBoundingClientRect()

    #    Get vertical middle
    hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2

    #    Determine mouse position
    clientOffset = monitor.getClientOffset()

    #    Get pixels to the top
    hoverClientY = clientOffset.y - hoverBoundingRect.top

    #    Dragging downwards
    if dragIndex < hoverIndex and hoverClientY < hoverMiddleY
      return false

    #    Dragging upwards
    if dragIndex > hoverIndex and hoverClientY > hoverMiddleY
      return false

    #    Time to actually perform the action
    props.moveInfo dragIndex, hoverIndex, columnIndex
    monitor.getItem().index = hoverIndex
    return

collect_drag = (connect, monitor) ->
  connectDragSource: connect.dragSource()
  connectDragPreview: connect.dragPreview()
  isDragging: monitor.isDragging()

collect_drop = (connect, monitor) ->
  connectDropTarget: connect.dropTarget()

BundleInfoItem = React.createClass
  PropTypes:
    connectDragSource: PropTypes.func.isRequired
    connectDragPreview: PropTypes.func.isRequired
    connectDropTarget: PropTypes.func.isRequired
    moveInfo: PropTypes.func.isRequired
    id: PropTypes.any.isRequired
    index: PropTypes.number.isRequired
    text: PropTypes.string.isRequired
    isDragging: PropTypes.bool.isRequired
    isOver: PropTypes.bool.isRequired

  getInitialState: ->
    input_value: @props.text

  handleChange: (event) ->
    @setState input_value: event.target.value.substr 0, 60

  handleOnBlur: ->
    if @props.text != @state.input_value
      @props.editContacts @props.column, @props.index, @state.input_value

  handleOnClick: ->
    @props.removeContacts @props.column, @props.index

  render: ->
    connectDragSource = @props.connectDragSource
    connectDragPreview = @props.connectDragPreview
    connectDropTarget = @props.connectDropTarget
    isDragging = @props.isDragging

    styles =
      display: 'none'

    connectDropTarget React.DOM.li
      className: if isDragging then 'info-section__placeholder' else 'info-section__wrapper-item '
      connectDragPreview React.DOM.div
        className: ''
        React.DOM.input
          style: if isDragging then styles
          className: 'input--bundle-man-info info-section__item'
          onChange: (event) => @handleChange(event)
          onBlur: @handleOnBlur
          value: @state.input_value
        connectDragSource React.DOM.i
          style: if isDragging then styles
          className: 'is is-' + @props.icon
      React.DOM.span
        className: 'info-section__del-item'
        onClick: @handleOnClick
        String.fromCharCode 215

BundleInfoItem = DragSource(ItemTypes.CARD, cardSource, collect_drag) BundleInfoItem
module.exports = DropTarget(ItemTypes.CARD, cardTarget, collect_drop) BundleInfoItem
