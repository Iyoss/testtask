'use strict'
React = require 'react'
update = require 'react/lib/update';
BundleHeaderPerson = require './Header.coffee'
BundleInfoPerson = require './Info.coffee'

BundlePerson = React.createClass
  getInitialState: ->
    is_show_bundle_info: true
    common_info:
      name: 'Lilo Stunriko'
      position: 'Manager'
      avatar: 'assets/img/avatars/2@3x.png'
      company:
        name: 'Google Corporation'
        url: 'https://www.google.pl'
      email: 'LiloStunriko@gmail.com'
      phone: '+1 (425) 882-8080'
      address: 'USA, Chicago, Semetery Road 7, 98054'
    additional_person_info: [
      {
        section: 'email'
        icon: 'email-light'
        type: 'mail'
        add_btn_text: 'Email'
        contacts: [
          {
            id: 1
            text: 'LiloStunriko@gmail.com'
          }
          {
            id: 2
            text: 'LiloStunriko@com'
          }
          {
            id: 3
            text: 'LiloStunriko@gmail.re'
          }
        ]
      }
      {
        section: 'phone'
        icon: 'phone'
        type: 'tel'
        add_btn_text: 'phone'
        contacts: [
          {
            id: 1
            text: '+1 (425) 336-8585'
          }
          {
            id: 2
            text: '+1 (595) 111-2222'
          }
          {
            id: 3
            text: '+1 (444) 555-6666'
          }
        ]
      }
      {
        section: 'address'
        icon: 'address'
        type: 'text'
        add_btn_text: 'Address'
        contacts: [
          {
            id: 1
            text: 'USA, Chicago, Semetery Road 7, 98054'
          }
          {
            id: 2
            text: 'EGD, Chicago, Semetery Road 7, 98054'
          }
        ]
      }
    ]

  moveInfo: (drag_index, hover_index, column) ->
    data = @state.additional_person_info
    common_info = @state.common_info

    section_text = common_info[data[column].section]
    cards = data[column].contacts
    dragCard = cards[drag_index]

    new_data = @state.additional_person_info.slice()
    new_data[column].contacts.splice drag_index, 1
    new_data[column].contacts.splice hover_index, 0, dragCard
    if section_text != new_data[column].contacts[0].text
      common_info[data[column].section] = new_data[column].contacts[0].text
      @setState common_info: common_info
    @setState additional_person_info: new_data


  editPersonContacts: (column, row, new_value) ->
    new_data = @state.additional_person_info
    new_data[column].contacts[row].text = new_value
    @setState additional_person_info: new_data

  addPersonContacts: (column, value) ->
    new_data = @state.additional_person_info
    max_id = 0
    for d, i in new_data[column].contacts
      if max_id < d.id
        max_id = d.id
    id = +max_id + 1
    new_data[column].contacts.push {id: id, text: value}
    @setState additional_person_info: new_data

  removePersonContacts: (column, index) ->
    new_data = @state.additional_person_info.slice()
    new_data[column].contacts.splice(index, 1)
    @setState additional_person_info: new_data

  toggleInfoSection: ->
    @setState is_show_bundle_info: !@state.is_show_bundle_info

  render: ->
    React.DOM.article
      className: 'discussion_group'
      React.DOM.div
        className: 'discussion open'
        React.DOM.div
          className: 'discussion_full bundle bundle-man'
          React.createElement BundleHeaderPerson,
            is_show_info: @state.is_show_bundle_info
            toggleInfoSection: @toggleInfoSection
            common_info: @state.common_info
          React.createElement BundleInfoPerson,
            is_show_info: @state.is_show_bundle_info
            info: @state.additional_person_info
            editContacts: @editPersonContacts
            moveInfo: @moveInfo
            addContacts: @addPersonContacts
            removeContacts: @removePersonContacts

module.exports = BundlePerson
