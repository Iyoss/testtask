'use strict'
React = require 'react'

BundleHeaderInfoPerson = React.createClass
  render: ->
    React.DOM.div
      className: 'left-col'
      React.DOM.div
        className: 'bundle-header__info'
        React.DOM.p
          className: 'item-text name'
          @props.common_info.name
        React.DOM.p
          className: 'item-text position'
          @props.common_info.position + ' at '
          React.DOM.a
            className: 'link'
            href: @props.common_info.company.url
            @props.common_info.company.name
      React.DOM.div
        className: 'bundle-header__info'
        React.DOM.p
          className: 'item-text'
          React.DOM.a
            href: 'mailto:' + @props.common_info.email
            @props.common_info.email
          React.DOM.i
            className: 'is is-email-light'
        React.DOM.p
          className: 'item-text'
          @props.common_info.phone
          React.DOM.i
            className: 'is is-phone'
      React.DOM.div
        className: 'bundle-header__info'
        React.DOM.p
          className: 'item-text'
          @props.common_info.address
          React.DOM.i
            className: 'is is-address'

module.exports = BundleHeaderInfoPerson
