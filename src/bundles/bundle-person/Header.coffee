'use strict'
React = require 'react'
HeaderActions = require '../components/HeaderActions.coffee'
BundleHeaderInfoPerson = require './HeaderInfo.coffee'

BundleHeaderPerson = React.createClass
  render: ->
    React.DOM.div
      className: 'bundle__header'
      React.createElement HeaderActions,
        is_show_info: @props.is_show_info
        toggleInfoSection: @props.toggleInfoSection
      React.DOM.img
        alt: 'user_avatar'
        className: 'avatar avatar--md pull-left'
        src: @props.common_info.avatar
      React.createElement BundleHeaderInfoPerson,
        common_info: @props.common_info

module.exports = BundleHeaderPerson
