'use strict'
React = require 'react'

HeaderActions = React.createClass
  render: ->
    style = 'discussion_actions__item_link open_info'
    style = style + ' active' if @props.is_show_info

    React.DOM.div
      className: 'bundle__header__right pull-right'
      React.DOM.div
        className: 'bundle__header__item discussion_full__actions'
        React.DOM.ul
          className: ''
          React.DOM.li
            className: 'discussion_full__actions__item'
            React.DOM.button
              className: style
              onClick: @props.toggleInfoSection
              React.DOM.i
                className: 'is is-actions-info'
                null
          React.DOM.li
            className: 'discussion_full__actions__item'
            React.DOM.button
              className: 'discussion_actions__item_link'
              React.DOM.i
                className: 'is is-email-small'
                null

module.exports = HeaderActions
