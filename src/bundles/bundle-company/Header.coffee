'use strict'
React = require 'react'
HeaderActions = require '../components/HeaderActions.coffee'
BundleHeaderInfoCompany = require './HeaderInfo.coffee'
BundleHeaderParticipantsCompany = require './HeaderParticipants.coffee'

BundleHeaderCompany = React.createClass
  render: ->
    React.DOM.div
      className: 'bundle__header'
      React.createElement HeaderActions,
        is_show_info: @props.is_show_info
        toggleInfoSection: @props.toggleInfoSection
      React.createElement BundleHeaderInfoCompany,
        company_info: @props.company_info
      React.createElement BundleHeaderParticipantsCompany,
        participants: @props.participants

module.exports = BundleHeaderCompany
