'use strict'
React = require 'react'

BundleHeaderParticipantsCompany = React.createClass
  createItems: ->
    for d, i in @props.participants
      React.DOM.a
        key: i
        className: 'bundle__participants-item'
        href: d.url
        React.DOM.img
          className: 'item-img'
          alt: d.alt
          src: d.avatar

  render: ->
    React.DOM.div
      className: 'bundle__participants'
      @createItems()

module.exports = BundleHeaderParticipantsCompany
