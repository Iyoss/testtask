'use strict'
React = require 'react'
BundleHeaderCompany = require './Header.coffee'
BundleInfoCompany = require './Info.coffee'

BundleCompany = React.createClass
  getInitialState: ->
    is_show_bundle_info: false
    information:
      company_info:
        name: 'Google, Inc.'
        owners: 'Alpabet Corp'
        country: 'USA, CHicago'
        address: 'One Google Way Redmond, WA 98052-7329'
      participants: [
        {
          url: '#'
          avatar: 'assets/img/avatars/1.jpg'
          alt: 'avatars_1'
        }
        {
          url: '#'
          avatar: 'assets/img/avatars/2.jpg'
          alt: 'avatars_2'
        }
        {
          url: '#'
          avatar: 'assets/img/avatars/3.jpg'
          alt: 'avatars_3'
        }
        {
          url: '#'
          avatar: 'assets/img/avatars/4.jpg'
          alt: 'avatars_4'
        }
        {
          url: '#'
          avatar: 'assets/img/avatars/5.jpg'
          alt: 'avatars_5'
        }
        {
          url: '#'
          avatar: 'assets/img/avatars/1.jpg'
          alt: 'avatars_6'
        }
      ]
      deal_info: [
        {
          invoice: 'Invoice #3050616'
          coin: '6,675,00 USD'
          date: '17 Feb, 2016'
          'man-light': 'Andrey Ocunev'
        }
        {
          invoice: 'Invoice #3059523'
          coin: '6,00,00 USD'
          date: '17 Feb, 2019'
          'man-light': 'Aleksander Ocunev'
        }
      ]

  toggleInfoSection: ->
    @setState is_show_bundle_info: !@state.is_show_bundle_info

  render: ->
    React.DOM.article
      className: 'discussion_group'
      React.DOM.div
        className: 'discussion open'
        React.DOM.div
          className: 'discussion_full bundle bundle-company'
          React.createElement BundleHeaderCompany,
            is_show_info: @state.is_show_bundle_info
            toggleInfoSection: @toggleInfoSection
            company_info: @state.information.company_info
            participants: @state.information.participants
          React.createElement BundleInfoCompany,
            is_show_info: @state.is_show_bundle_info
            deal_info: @state.information.deal_info

module.exports = BundleCompany
