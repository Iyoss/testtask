'use strict'
React = require 'react'

BundleInfoCompany = React.createClass
  createItems:(info) ->
    key = 0
    for i of info
      style = 'is is-' + i
      key++
      React.DOM.p
        key: key
        className: 'info-section-item__col col'
        info[i]
        React.DOM.i
          className: style
          null

  createRows: ->
    for d, i in @props.deal_info
      React.DOM.li
        key: i
        className: 'info-section__wrapper-item'
        React.DOM.div
          className: 'info-section__item'
          @createItems(d)

  render: ->
    style = 'bundle__info'
    style = 'bundle__info open' if @props.is_show_info

    React.DOM.div
      className: style
      React.DOM.div
        className: 'bundle-info__section'
        React.DOM.ul
          className: ''
          @createRows()

module.exports = BundleInfoCompany
