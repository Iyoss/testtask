'use strict'
React = require 'react'

BundleHeaderInfoCompany = React.createClass
  render: ->
    React.DOM.div
      className: 'left-col'
      React.DOM.div
        className: 'bundle-header__info'
        React.DOM.p
          className: 'item-text name'
          @props.company_info.name
        React.DOM.p
          className: 'item-text owners'
          @props.company_info.owners
      React.DOM.div
        className: 'bundle-header__info'
        React.DOM.p
          className: 'item-text'
          @props.company_info.country
          React.DOM.i
            className: 'is is-address'
            null
        React.DOM.p
          className: 'item-text'
          @props.company_info.address

module.exports = BundleHeaderInfoCompany
