'use strict'
React = require 'react'

meaning_estimations = [2 ,4 , 6, 8, 16, 24, 32];

Estimation = React.createClass
  onClick: (num) ->
    new_val = 0
    new_val = num if num != @props.estimation_state
    @props.changeEstimation(new_val)

  createItems: ->
    style_item_active = 'label_estimation__item active'

    for i in [1...7]
      React.DOM.div
        key: i
        className:
          if @props.estimation_state != i
            'label_estimation__item'
          else style_item_active
        onClick: do (i) => () => @onClick i
        React.DOM.button
          className: 'label_estimation__item-button'
          React.DOM.span
            className: 'label_estimation__item-number'
            meaning_estimations[i]

  render: ->
    React.DOM.div
      className: 'modal_label_list__item label-estimation'
      React.DOM.h5
        className: 'modal_label_list__item-title'
        'Estimation'
      React.DOM.div
        className: 'label_estimation__holder'
        @createItems()

module.exports = Estimation
