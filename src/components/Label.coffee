'use strict'
React = require 'react'

style_arr = [
  'label_colors__holder',
  'now_changing'
]

Label = React.createClass
  onClick: (color) ->
    new_val = ''
    new_val = color if color != @props.label_state
    @props.changeLabel(new_val)

  createItems: ->
    style_item_active = 'label_colors__item-button active'

    for i in [1...12]
      React.DOM.div
        key: i
        className: 'label_colors__item'
        React.DOM.button
          'data-color': 'color' + i
          className:
            if @props.label_state != ('color' + i)
              'label_colors__item-button'
            else style_item_active
          onClick: do (i) => () => @onClick 'color' + i
          ''

  render: ->
    style =
      if @props.label_state
      then style_arr.join(' ')
      else 'label_colors__holder'

    React.DOM.div
      className: 'modal_label_list__item label_colors'
      React.DOM.h5
        className: 'modal_label_list__item-title'
        'Label'
      React.DOM.div
        className: style
        @createItems()

module.exports = Label
