'use strict'
React = require 'react'
Label = require './Label.coffee'
Priority = require './Priority.coffee'
Estimation = require './Estimation.coffee'

style_arr = [
  'popup_panel'
  'submenu-label'
  'submenu'
]

Modal = React.createClass
  getInitialState: ->
    label: ''
    priority: 0
    estimation: 0

  changeLabel: (new_val) ->
    @setState label: new_val

  changePriority: (new_val) ->
    @setState priority: new_val

  changeEstimation: (new_val) ->
    @setState estimation: new_val

  render: ->
    if style_arr.length == 3 && @props.modal_show_state
      style_arr.push('open')
    else if style_arr.length > 3 && !@props.modal_show_state
      style_arr.pop()
    style = style_arr.join(' ')

    React.DOM.div
      className: style
      style:
        marginLeft: 100
      React.DOM.div
        className: 'modal_label_list'
        React.createElement Label,
          label_state: @state.label
          changeLabel: @changeLabel
        React.createElement Priority,
          label_state: @state.label
          priority_state: @state.priority
          changePriority: @changePriority
        React.createElement Estimation,
          estimation_state: @state.estimation
          changeEstimation: @changeEstimation

module.exports = Modal
