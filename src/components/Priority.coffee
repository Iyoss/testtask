'use strict'
React = require 'react'

Priority = React.createClass
  onClick: (num) ->
    new_val = 0
    new_val = num if num != @props.priority_state
    @props.changePriority(new_val)

  createItems: ->
    style_item_active = 'label_priority__item active'

    for i in [1...12]
      React.DOM.div
        key: i
        className:
          if @props.priority_state >= i
            style_item_active
          else 'label_priority__item'
        onClick: do (i) => () => @onClick i
        React.DOM.button
          className: 'label_priority__item-button'
          React.DOM.span
            className: 'label_priority__item-number'
            i

  render: ->
    style = [
      'label_priority__holder'
      @props.label_state
    ]
    style = style.join(' ')

    React.DOM.div
      className: 'modal_label_list__item label-priority'
      React.DOM.h5
        className: 'modal_label_list__item-title'
        'Priority'
      React.DOM.div
        className: style
        @createItems()

module.exports = Priority
