'use strict'
React = require 'react'
onClickOutside = require 'react-onclickoutside'
Modal = require './Modal.coffee'

style_arr = [
  'discussion_sorting'
  'show'
]

App = React.createClass
  autobind: false

  getInitialState: ->
    modal_show: false

  handleClickOutside: (e) ->
    @closeModalWindow()

  toggleModalWindow: ->
    @setState modal_show: !@state.modal_show

  closeModalWindow: ->
    @setState modal_show: false

  render: ->
    style =
      if @state.modal_show
      then style_arr.join(' ')
      else 'discussion_sorting'
    style_button =
      width: 50
      fontSize: 16
      color: '#000'
      borderWidth: 1
      borderColor: '#000'
      borderStyle: 'solid'
      cursor: 'pointer'
    React.DOM.div
      className: style
      React.DOM.div
        style : style_button
        onClick: @toggleModalWindow.bind(@)
        'Push'
      React.createElement Modal, modal_show_state: @state.modal_show

App = onClickOutside App

module.exports = App
